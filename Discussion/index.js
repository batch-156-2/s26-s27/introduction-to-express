// Create a server using Express.js

// 1. Use the require() directive to acquire the express library and its utilities.
// 2. Create a server using only express.
	// Express will allow us to create an express application.
// 3. Identify a port/address that will serve/host this newly created connection/app.
// 4. Bind the application to the desired designated port using the listen(). Create a method that will send a response that the connection has been established.

const express = require('express');
const server = express();
const address = 3000;
server.listen(address, () => {
	console.log(`Server is running on port: ${address}`);
});